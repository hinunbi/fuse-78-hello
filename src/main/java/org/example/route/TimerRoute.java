package org.example.route;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.spring.SpringRouteBuilder;
import org.springframework.stereotype.Component;

@Component(value = "timerRoute")
public class TimerRoute extends SpringRouteBuilder {
  @Override
  public void configure() throws Exception {
    from("timer:test?period=1000")
        .log("${date:now}")
        ;
  }
}
